import { ICommand } from '@nestjs/cqrs';
import { StudentDto } from '../../entity/student/student-dto';

export class UpdateStudentCommand implements ICommand {
  constructor(public readonly updatePayload: StudentDto) {}
}
