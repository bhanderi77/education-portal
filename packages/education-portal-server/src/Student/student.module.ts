import { Module, HttpModule } from '@nestjs/common';
import { StudentAggregatesManager } from './aggregates';
import { StudentEntitiesModule } from './entity/entity.module';
import { StudentQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { StudentCommandManager } from './command';
import { StudentEventManager } from './event';
import { StudentController } from './controllers/student/student.controller';
import { StudentPoliciesService } from './policies/student-policies/student-policies.service';

@Module({
  imports: [StudentEntitiesModule, CqrsModule, HttpModule],
  controllers: [StudentController],
  providers: [
    ...StudentAggregatesManager,
    ...StudentQueryManager,
    ...StudentEventManager,
    ...StudentCommandManager,
    StudentPoliciesService,
  ],
  exports: [StudentEntitiesModule],
})
export class StudentModule {}
