import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { StudentDto } from '../../entity/student/student-dto';
import { AddStudentCommand } from '../../command/add-student/add-student.command';
import { RemoveStudentCommand } from '../../command/remove-student/remove-student.command';
import { UpdateStudentCommand } from '../../command/update-student/update-student.command';
import { RetrieveStudentQuery } from '../../query/get-student/retrieve-student.query';
import { RetrieveStudentListQuery } from '../../query/list-student/retrieve-student-list.query';

@Controller('student')
export class StudentController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() generalPayload: StudentDto, @Req() req) {
    return this.commandBus.execute(new AddStudentCommand(generalPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveStudentCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid, @Req() req) {
    return await this.queryBus.execute(new RetrieveStudentQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveStudentListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: StudentDto) {
    return this.commandBus.execute(new UpdateStudentCommand(updatePayload));
  }
}
