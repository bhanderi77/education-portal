import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { StudentDto } from '../../entity/student/student-dto';
import { Student } from '../../entity/student/student.entity';
import { StudentAddedEvent } from '../../event/student-added/student-added.event';
import { StudentService } from '../../entity/student/student.service';
import { StudentRemovedEvent } from '../../event/student-removed/student-removed.event';
import { StudentUpdatedEvent } from '../../event/student-updated/student-updated.event';

@Injectable()
export class StudentAggregateService extends AggregateRoot {
  constructor(private readonly studentService: StudentService) {
    super();
  }

  addStudent(studentPayload: StudentDto, clientHttpRequest) {
    const student = new Student();
    student.uuid = uuidv4();
    Object.assign(student, studentPayload);
    this.apply(new StudentAddedEvent(student, clientHttpRequest));
  }

  async retrieveStudent(uuid: string, req) {
    const provider = await this.studentService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getStudentList(offset, limit, sort, search, clientHttpRequest) {
    return this.studentService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.studentService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new StudentRemovedEvent(found));
  }

  async update(updatePayload: StudentDto) {
    const provider = await this.studentService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new StudentUpdatedEvent(update));
  }
}
